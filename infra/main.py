#!/usr/bin/env python
import os
from unicodedata import name
from constructs import Construct
from cdktf import App, TerraformStack, TerraformOutput, HttpBackend
from cdktf_cdktf_provider_aws import AwsProvider
from cdktf_cdktf_provider_aws import vpc as _vpc
from cdktf_cdktf_provider_aws import ec2 as _ec2

GITLAB_PID = os.getenv("CI_PROJECT_ID")
GITLAB_USER = "gitlab-ci-token"
GITLAB_PASS = os.getenv("CI_JOB_TOKEN")
ENV = os.getenv("ENV")
PUBLIC_SSH_KEY = os.getenv("PUBLIC_SSH_KEY")


class NetworkStack(TerraformStack):
    def __init__(self, scope: Construct, ns: str):

        self.env = ""

        if ENV != "prod":
            self.env = f"{ENV}-"

        super().__init__(scope, ns)

        AwsProvider(self, "Aws", region="us-east-1")

        HttpBackend(
            self,
            address=f"https://gitlab.com/api/v4/projects/{GITLAB_PID}/terraform/state/{ENV}",
            lock_address=f"https://gitlab.com/api/v4/projects/{GITLAB_PID}/terraform/state/{ENV}/lock",
            unlock_address=f"https://gitlab.com/api/v4/projects/{GITLAB_PID}/terraform/state/{ENV}/lock",
            username=GITLAB_USER,
            password=GITLAB_PASS,
            lock_method="POST",
            unlock_method="DELETE",
            retry_wait_min=5,
        )

        keys = [1]

        for key in keys:
            net = _vpc.Vpc(
                self,
                f"{self.env}CustomVpc{key}",
                cidr_block=f"10.{key}.0.0/16",
                tags={"Name": f"{self.env}CustomVpc{key}"},
                enable_dns_hostnames=True,
                enable_dns_support=True,
            )

            igw = _vpc.InternetGateway(self, f"{self.env}IGW_{key}", vpc_id=net.id)

            route = _vpc.RouteTableRoute(
                cidr_block="0.0.0.0/0", gateway_id=igw.id, core_network_arn=""
            )

            route_table = _vpc.RouteTable(
                self, f"{self.env}Route_{key}", vpc_id=net.id, route=[route]
            )

            subnet_a = _vpc.Subnet(
                self,
                f"{self.env}CustomSubnet{key}a",
                vpc_id=net.id,
                cidr_block=f"10.{key}.1.0/24",
                availability_zone="us-east-1a",
                map_public_ip_on_launch=True,
            )

            subnet_b = _vpc.Subnet(
                self,
                f"{self.env}CustomSubnet{key}b",
                vpc_id=net.id,
                cidr_block=f"10.{key}.2.0/24",
                availability_zone="us-east-1b",
                map_public_ip_on_launch=True,
            )

            _vpc.RouteTableAssociation(
                self,
                f"{self.env}RTAssociation{key}_a",
                subnet_id=subnet_a.id,
                route_table_id=route_table.id,
            )

            _vpc.RouteTableAssociation(
                self,
                f"{self.env}RTAssociation{key}_b",
                subnet_id=subnet_b.id,
                route_table_id=route_table.id,
            )

            ## Get latest EC2 AMI for Instances

            ami_filter1 = _ec2.DataAwsAmiFilter(
                name="name",
                values=["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"],
            )
            ami_filter2 = _ec2.DataAwsAmiFilter(
                name="virtualization-type", values=["hvm"]
            )

            ami = _ec2.DataAwsAmi(
                self,
                f"{self.env}instance_{key}_ami",
                filter=[ami_filter1, ami_filter2],
                owners=["099720109477"],
                most_recent=True,
            )

            ## Create Security Groups

            ingress = _vpc.SecurityGroupIngress(
                cidr_blocks=["0.0.0.0/0"],
                from_port=22,
                to_port=22,
                protocol="TCP",
                description="Permit SSH Inbound for Ansible",
            )

            egress = _vpc.SecurityGroupEgress(
                cidr_blocks=["0.0.0.0/0"],
                from_port=0,
                to_port=0,
                protocol="-1",
                description="Permit All Outbound",
            )

            sg = _vpc.SecurityGroup(
                self,
                f"{self.env}securityGroup{key}",
                ingress=[ingress],
                egress=[egress],
                description="Limit access to instances",
                vpc_id=net.id,
            )

            ## Create SSH Keys

            ssh_key = _ec2.KeyPair(
                self,
                f"{self.env}keypair_{key}",
                public_key=PUBLIC_SSH_KEY,
                key_name=f"{self.env}AnsibleSSHKey",
            )

            ## Create Instances

            instance_a = _ec2.Instance(
                self,
                f"{self.env}instance_{key}_a",
                ami=ami.id,
                key_name=ssh_key.key_name,
                instance_type="t2.micro",
                associate_public_ip_address=True,
                subnet_id=subnet_a.id,
                vpc_security_group_ids=[sg.id],
                tags={
                    "Role": f"frontend",
                    "Name": f"{self.env}instance_{key}_a",
                    "Env": f"{ENV}"
                },
            )

            instance_aa = _ec2.Instance(
                self,
                f"{self.env}instance_{key}_aa",
                ami=ami.id,
                key_name=ssh_key.key_name,
                instance_type="t2.micro",
                associate_public_ip_address=True,
                subnet_id=subnet_a.id,
                vpc_security_group_ids=[sg.id],
                tags={
                    "Role": f"frontend",
                    "Name": f"{self.env}instance_{key}_aa",
                    "Env": f"{ENV}"
                },
            )

            instance_b = _ec2.Instance(
                self,
                f"{self.env}instance_{key}_b",
                ami=ami.id,
                key_name=ssh_key.key_name,
                instance_type="t2.micro",
                associate_public_ip_address=True,
                subnet_id=subnet_b.id,
                vpc_security_group_ids=[sg.id],
                tags={
                    "Role": f"backend",
                    "Name": f"{self.env}instance_{key}_b",
                    "Env": f"{ENV}"
                },
            )

            TerraformOutput(self, f"{self.env}vpc_{key}_id", value=net.id)
            TerraformOutput(self, f"{self.env}subnet_{key}a_id", value=subnet_a.id)
            TerraformOutput(self, f"{self.env}subnet_{key}b_id", value=subnet_b.id)
            TerraformOutput(self, f"{self.env}instance_{key}a_id", value=instance_a.id)
            TerraformOutput(self, f"{self.env}instance_{key}aa_id", value=instance_aa.id)
            TerraformOutput(self, f"{self.env}instance_{key}b_id", value=instance_b.id)

app = App()
NetworkStack(app, "python-aws")
app.synth()
